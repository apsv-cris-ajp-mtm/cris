package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String scopusURL = "https://www.scopus.com/authid/detail.uri?authorId=" + id;
		Client client = ClientBuilder.newClient(new ClientConfig());
		
		//Check if we call the servlet as an admin
		Researcher user = (Researcher) request.getSession().getAttribute("user");
		String userId = user.getId();
		if (!userId.equals("root")) {
			//Set message
			request.setAttribute("message", "Researchers can only be created by the Admin");
			//Finally forwarding to the Login View
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
			return;
		}
		
		//Check if the id does not exist yet
		//Receive the JSON and convert to Researcher object
		try {
			Researcher researcher = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" + id)
					.request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);
			if (researcher != null) {
				//Set message
				request.setAttribute("message", "User Id already exists");
				//Finally redirecting to the Admin Servlet
				response.sendRedirect(request.getContextPath() + "/AdminServlet");
				return;
			}
		} catch (Exception e){}
		
		
		//Create new researcher object from the params
		Researcher researcher_new = new Researcher();
		researcher_new.setId(id);
		researcher_new.setName(name);
		researcher_new.setLastname(lastname);
		researcher_new.setScopusURL(scopusURL);
		
		try {
			//We start a client and send the researcher to create to the rest service
			client.register(JsonProcessingFeature.class)
					.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/").request()
					.post(Entity.entity(researcher_new, MediaType.APPLICATION_JSON), Response.class);
		} catch (Exception e) {}
		
		//Finally redirecting to the ADMIN Servlet
		response.sendRedirect(request.getContextPath() + "/AdminServlet");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
