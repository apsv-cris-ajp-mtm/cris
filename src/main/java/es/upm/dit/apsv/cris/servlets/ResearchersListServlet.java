package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/ResearchersListServlet") //URL to which the servlet is connected to
public class ResearchersListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	//Creates a REST Client to attack Researchers, obtaining the list of researchers as a JSON
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Client client = ClientBuilder.newClient(new ClientConfig());
        
        //Receive the JSON and convert to Researchers List object
		List<Researcher> researchersList  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers")
				.request().accept(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Researcher>>() {});
		//Received object is set as an attribute of the request (transitory object)
		request.setAttribute ("researchersList", researchersList);
		//We pass the JSP view of the researchers list
		getServletContext().getRequestDispatcher("/ResearchersListView.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Redirects to the previous method
		doGet(request, response);
	}
}
