package es.upm.dit.apsv.cris.servlets;

public class URLHelper {
	//To guarantee that there is only a single object of this class in memory
	private static URLHelper instance = null;
	private String restUrl;
	
	public URLHelper() {

	    String envValue = System.getenv("CRISSERVICE_SRV_SERVICE_HOST");
	    if(envValue == null) //not in Kubernetes
	    	restUrl = "http://localhost:8080/CRISSERVICE";
	    else //In k8, DNS service resolution in Kubernetes
	    	restUrl = "http://crisservice-srv/CRISSERVICE";
	}
	
	//To access the single instance of this class
	public static URLHelper getInstance() {
		//Only if the instance hasnt been created previously it creates one
		if (null == instance)
			instance = new URLHelper();
		//And returns the single instance of the object
		return instance;
	}

	public String getCrisURL() {
	    return restUrl;
	}

}