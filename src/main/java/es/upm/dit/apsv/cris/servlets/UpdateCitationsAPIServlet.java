package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

@WebServlet("/UpdateCitationsAPIServlet")
public class UpdateCitationsAPIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Retrieve id parameter from the request
		String id = (String) request.getParameter("id");
		Client client = ClientBuilder.newClient(new ClientConfig());
        
        //Ask CRISSERVICE to update the citations of the given publication id
		client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/" + id + "/UpdateCiteNumber").request().get();
		//Finally redirecting to the Publication Servlet
		response.sendRedirect(request.getContextPath() + "/PublicationServlet?id=" + id);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
