package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.net.URLEncoder;

import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;      

	final String ADMIN = "root";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
	}

	@Override //Checks login parameters: email and password
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Researcher researcher = null;
		
		//If the researcher is the ADMIN of the system
		if (ADMIN.equals(email) && ADMIN.equals(password)) {
			//We create a new user whose id is root
			researcher = new Researcher();
			researcher.setId("root");
			//And we store it inside the session
			request.getSession().setAttribute("user", researcher);
			//Finally redirecting to the ADMIN Servlet
			response.sendRedirect(request.getContextPath() + "/AdminServlet");
			//Return to avoid doing more things
			return;
		}
		
		//If we log in with a normal user
		try {
			//We start a client and send the login info to Moodle
			Client client = ClientBuilder.newClient(new ClientConfig());
			Response resp = client.register(JsonProcessingFeature.class)
					.target("https://moodle.upm.es/titulaciones/oficiales/"+"login/token.php"+
							"?username="+URLEncoder.encode(email,"UTF-8")+
							"&password="+URLEncoder.encode(password,"UTF-8")+
							"&service=moodle_mobile_app").request()
					.post(Entity.entity(researcher, MediaType.APPLICATION_JSON), Response.class);
			//Receiving a token as a response
			String token = resp.readEntity(JsonObject.class).get("token").toString();
			//If the token is not null or empty, we loged in successfully, and we request the researcher's data by email to CRISSERVICE
			if ((token != null) && (token.length() > 0)) {
				researcher = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/email/")
						.queryParam("email", email).request()
						.accept(MediaType.APPLICATION_JSON).get(Researcher.class);
			}
		} catch(Exception e) {}
		
		
		if (null != researcher) { //If we managed to obtain a researcher with the email, we redirect to the Researcher servlet
			request.getSession().setAttribute("user", researcher);
			response.sendRedirect(request.getContextPath() + "/ResearcherServlet" + "?id=" + researcher.getId());
		} else { //If something failed, we return an error message asking the user to try again 
			//Delete any active object from the session
			request.getSession().invalidate();
			request.setAttribute("message", "Invalid user or password");
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
	}

}
