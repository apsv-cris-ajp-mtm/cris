package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/ResearcherServlet")
public class ResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	//Creates a REST Client to attack Researchers, obtaining the list of researchers as a JSON
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Retrieve id parameter from the request
		String id = (String) request.getParameter("id");
		Client client = ClientBuilder.newClient(new ClientConfig());
		
		//Receive the JSON and convert to Researcher object
		Researcher researcher = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" + id)
				.request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);
		
		//Receive the JSON and convert to Publications list
		List<Publication> publications = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" + id + "/Publications")
						.request().accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Publication>>() {});
		
		if (researcher == null) {
			//Do something if the researcher with the specified id is not found
		}
		
		//Received objects are set as attributes of the request (transitory object)
		request.setAttribute ("researcher", researcher);
		request.setAttribute ("publications", publications);
		
		//We pass the JSP view of the researcher
		getServletContext().getRequestDispatcher("/ResearcherView.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
