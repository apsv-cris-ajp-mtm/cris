<div class="hero-body">

<c:if test="${not (empty user)}">
	<div class='box has-text-success'>
		<h2 class="subtitle is-4">You are authenticated as ${user.id}</h2>
		<a class="button is-link is-danger" href="LogoutServlet">Logout</a>
		<c:if test="${user.id == 'root'}">
			<a class="button is-link is-success" href="AdminServlet">Admin Panel</a>
		</c:if>
	</div>
</c:if>

<c:if test="${empty user}">
	<div class='box has-text-info'>
		<h2 class="subtitle is-4">You are not logged in</h2>
		<a class="button is-link is-info" href="LoginServlet">Login</a>
	</div>
</c:if>

<c:if test="${not (empty message)}">
	<div class='box has-text-danger'>
	    <p>${message}</p>
	</div>
</c:if>

<h3>
	<a class="is-size-3" href="ResearchersListServlet">Researchers list</a>
</h3>

</div>
