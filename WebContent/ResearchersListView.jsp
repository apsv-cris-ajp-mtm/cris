<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Researchers List</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.0/css/bulma.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
</head>
<body>
<div class="hero-body">
	<table class="table is-bordered is-striped is-hoverable is-fullwidth">

		<tr>
			<th>Id</th><th>Name</th><th>Last name</th><th>URL</th><th>Email</th>
		</tr>
		
		<c:forEach items="${researchersList}" var="researcher">
		        <tr>
					<td> <a href="ResearcherServlet?id=${researcher.id}"> ${researcher.id} </a></td>
	                <td>${researcher.name}</td>
	                <td>${researcher.lastname}</td>
	                <td><a href="${researcher.scopusURL}">${researcher.scopusURL}</a></td>
	                <td>${researcher.email}</td>
	        	</tr>
		</c:forEach>

	</table>
</div>
</body>
</html>