<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Publication</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.0/css/bulma.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
</head>
<body>
<div class="hero-body">
	<div class="box">
		<div class="columns">
			<div class="column">
				<h1 class="title is-3">Publication's Info</h1>
			</div>
			<div class="column is-narrow">
				<a class="button is-link is-primary" href="UpdateCitationsAPIServlet?id=${publication.id}">Update Cite Count</a>
			</div>
		</div>
		<h1 class="subtitle is-2">${publication.title}</h1>
	</div>
	
	<table class="table is-bordered is-striped is-hoverable is-fullwidth">
		<tr>
			<th>Title</th><th>Cite Count</th><th>Publication Date</th><th>Publication Name</th>
		</tr>
		
		<tr>
			<td>${publication.title} </a></td>
            <td>${publication.citeCount}</td>
            <td>${publication.publicationDate}</td>
            <td>${publication.publicationName}</td>
		</tr>
		
	</table>
	
	<table class="table is-bordered is-striped is-hoverable is-fullwidth">
		<tr>
			<th>Researchers Id</th><th>Name</th><th>Last Name</th>
		</tr>
		
		<c:forEach items="${researchers}" var="researcher">
		        <tr>
	                <td><a href="ResearcherServlet?id=${researcher.id}">${researcher.id}</a></td>
	                <td>${researcher.name}</td>
	                <td>${researcher.lastname}</td>
	        	</tr>
		</c:forEach>

	</table>
	
</div>
</body>
</html>