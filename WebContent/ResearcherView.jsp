<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Researcher</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.0/css/bulma.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
</head>
<body>
<div class="hero-body">
	<div class="box">
		<div class="columns">
			<div class="column">
				<h1 class="title is-3">Researcher's Info</h1>
			</div>
			<div class="column is-narrow">
				<a class="button is-link is-primary" href="UpdatePublicationsQueueServlet?id=${researcher.id}">Update Publications</a>
			</div>
		</div>
		<h1 class="subtitle is-2">${researcher.name} ${researcher.lastname}</h1>
	</div>
	
	
	<cp:if test="${researcher.id == user.id}">
		<h2 class="subtitle is-3">Use the form below to add a new publication!</h2>
		<form action="CreatePublicationServlet" method="get">
			<div class="field">
				<input type="text" class="input" name="id" placeholder="Publication Id">
			</div>
			<div class="field">
				<input type="text" class="input" name="title" placeholder="Title">
			</div>
			<div class="field">
				<input type="text" class="input" name="authors" placeholder="Authors: AuthorId1;AuthorId2;AuthorId3...">
			</div>
			<div class="field">
				<input type="text" class="input" name="publicationDate" placeholder="Date: YEAR-MO-DA">
			</div>
			<div class="field">
				<input type="text" class="input" name="publicationName" placeholder="Publication Name">
			</div>
			<div class="field">
				<button type="submit" class="button is-link">Create Publication</button>
			</div>
		</form>
	</cp:if>
	
	<table class="table is-bordered is-striped is-hoverable is-fullwidth">
		<tr>
			<th>Id</th><th>Name</th><th>Last name</th><th>URL</th><th>Email</th>
		</tr>
		
		<tr>
			<td>${researcher.id} </a></td>
            <td>${researcher.name}</td>
            <td>${researcher.lastname}</td>
            <td><a href="${researcher.scopusURL}">${researcher.scopusURL}</a></td>
            <td>${researcher.email}</td>
		</tr>
		
	</table>
	
	<table class="table is-bordered is-striped is-hoverable is-fullwidth">

		<th class="has-text-left">Publications</th><th>Title</th><th>Cite Count</th><th>Publication Date</th><th>Publication Name</th>
		
		<c:forEach items="${publications}" var="publication">
		        <tr>
	                <td><a href="PublicationServlet?id=${publication.id}">${publication.id}</a></td>
	                <td>${publication.title} </a></td>
	                <td>${publication.citeCount}</td>
		            <td>${publication.publicationDate}</td>
		            <td>${publication.publicationName}</td>
	        	</tr>
		</c:forEach>

	</table>
	
</div>
</body>
</html>