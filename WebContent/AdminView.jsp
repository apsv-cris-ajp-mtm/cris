<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Admin</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.0/css/bulma.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
</head>
<body>
<div class="hero-body">
	<h2 class="subtitle is-2">Hi Admin!</h2>
	
	<h2 class="subtitle is-3">Use the form below to create a new researcher!</h2>
	<form action="CreateResearcherServlet" method="get">
		<div class="field">
			<input type="text" class="input" name="id" placeholder="User Id">
		</div>
		<div class="field">
			<input type="text" class="input" name="name" placeholder="Name">
		</div>
		<div class="field">
			<input type="text" class="input" name="lastname" placeholder="Last Name">
		</div>
		<div class="field">
			<input type="text" class="input" name="email" placeholder="Email">
		</div>
		<div class="field">
			<button type="submit" class="button is-link">Create Researcher</button>
		</div>
	</form>

</div>
</body>
</html>